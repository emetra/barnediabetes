CREATE SCHEMA BDR AUTHORIZATION dbo;
GO

CREATE TABLE BDR.Variable ( 
  VarId INT NOT NULL IDENTITY(101,1) PRIMARY KEY, 
  VarName VARCHAR(24) NOT NULL, 
  VarClass VARCHAR(16) NOT NULL,
  IsLabData BIT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT GETDATE(),
  CreatedBy INT NOT NULL REFERENCES dbo.UserList( UserId) );
GO

CREATE TABLE BDR.VariableOption ( 
  RowId INT NOT NULL IDENTITY (1001,1) PRIMARY KEY,
  VarId INT NOT NULL, 
  OptionValue INT NOT NULL, 
  OptionText VARCHAR(64) NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT GETDATE(),
  CreatedBy INT NOT NULL REFERENCES dbo.UserList( UserId) );
GO

CREATE TABLE BDR.Context( 
  ContextId INT NOT NULL PRIMARY KEY, 
  ContextName VARCHAR(64) NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT GETDATE(),
  CreatedBy INT NOT NULL REFERENCES dbo.UserList( UserId) );
GO

CREATE TABLE BDR.VariableContext( 
  RowId INT NOT NULL IDENTITY (1,1) PRIMARY KEY,
  ContextId INT NOT NULL, 
  VarId INT NOT NULL,
  CreatedAt DATETIME NOT NULL DEFAULT GETDATE(),
  CreatedBy INT NOT NULL DEFAULT USER_ID() REFERENCES dbo.UserList( UserId) );
GO

INSERT INTO BDR.Context ( ContextId, ContextName ) VALUES ( 1, 'Famileanamnese' )
INSERT INTO BDR.Context ( ContextId, ContextName ) VALUES ( 2, 'Pasientsvar foresatte skjema' )
INSERT INTO BDR.Context ( ContextId, ContextName ) VALUES ( 3, 'Pasientsvar barn' )
INSERT INTO BDR.Context ( ContextId, ContextName ) VALUES ( 4, 'Årskontroll' )
INSERT INTO BDR.Context ( ContextId, ContextName ) VALUES ( 5, 'Førstegangsregistering' )

CREATE UNIQUE INDEX UC_BDR_Variable_VarName ON BDR.Variable( VarName );
CREATE UNIQUE INDEX UC_BDR_VariableContext_ContextId_VarId ON BDR.VariableContext( ContextId, VarId );
CREATE UNIQUE INDEX UC_BDR_VariableOption_VarId_OptionValue ON BDR.VariableOption( VarId, OptionValue );

GO
ALTER PROCEDURE BDR.AddItem( @VarName VARCHAR(24), @VarClass VARCHAR(16), @ContextId INT ) AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @VarId INT;
  BEGIN TRY
     INSERT INTO BDR.Variable ( VarName, VarClass ) VALUES ( @VarName, @VarClass );
  END TRY
  BEGIN CATCH
    PRINT CONCAT( 'Variabelen ', @VarName, ' ble ikke lagret' )
  END CATCH;
  SELECT @VarId = VarId FROM BDR.Variable WHERE VarName = @VarName;
  INSERT INTO BDR.VariableContext ( VarId, ContextId ) VALUES( @VarId, @ContextId );
END;
GO
ALTER PROCEDURE BDR.AddItemOption( @VarName VARCHAR(24), @OptionValue INT, @OptionText VARCHAR(64) ) AS
BEGIN
  SET NOCOUNT ON;
  DECLARE @VarId INT;
  SELECT @VarId = VarId FROM BDR.Variable WHERE VarName = @VarName;
  INSERT INTO BDR.VariableOption( VarId, OptionValue, OptionText ) VALUES( @VarId, @OptionValue, @OptionText );
END;

ALTER TABLE BDR.Variable ADD ItemId INT NULL REFERENCES dbo.MetaItem( ItemId );
GO
CREATE PROCEDURE BDR.AddItemMap( @ItemId INT, @VarName VARCHAR(24) ) AS 
BEGIN
  SET NOCOUNT ON;
  UPDATE BDR.Variable SET ItemId = @ItemId WHERE VarName = @VarName;
END;
GO

ALTER TABLE BDR.Variable ADD NeedsMapping BIT NULL;

UPDATE BDR.Variable SET NeedsMapping = 0 
WHERE VarName  iN ( 'SkjemaGuid', 'PasientGuid', 'PasientGUID', 'Skjematype', 'HovedskjemaGUID' );

GO
ALTER VIEW BDR.VariablesThatNeedMapping AS
  SELECT c.ContextId, c.ContextName, v.VarId, v.VarText, v.VarName, v.VarClass, v.IsLabData
  FROM BDR.Context c
    JOIN BDR.VariableContext vc ON vc.ContextId = c.ContextId
    JOIN BDR.Variable v ON v.VarId = vc.VarId
  WHERE v.ItemId IS NULL AND ISNULL( v.NeedsMapping, 1 ) = 1 AND c.ContextId IN ( 1, 4, 5 )
GO

CREATE VIEW BDR.VariablesWithoutMapping AS
  SELECT c.ContextId, c.ContextName, v.VarId, v.VarText, v.VarName, v.VarClass
  FROM BDR.Context c
    JOIN BDR.VariableContext vc ON vc.ContextId = c.ContextId
    JOIN BDR.Variable v ON v.VarId = vc.VarId
  WHERE v.ItemId IS NULL AND ISNULL( v.NeedsMapping, 1 ) = 1;
GO

CREATE TABLE BDR.EnumReplacement ( 
  RowId INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
  ItemId INT NOT NULL REFERENCES dbo.MetaItem( ItemId ),
  OurEnumVal INT NOT NULL,
  TheirEnumVal INT NOT NULL );
GO

CREATE PROCEDURE BDR.AddEnumReplacement( @ItemId INT, @OurEnumVal INT, @TheirEnumVal INT ) AS
BEGIN
  INSERT INTO BDR.EnumReplacement( ItemId, OurEnumVal, TheirEnumVal )
  VALUES( @ItemId, @OurEnumVal, @TheirEnumVal );
END
GO


-- Examples of use


SELECT * from BDR.Variable where ItemId IS NULL ORDER by VarName

select * from BDR.Variable ORDER BY VarId;

UPDATE BDR.Variable SET NeedsMapping = 0 WHERE VarId IN ( 105,106,107, 486, 672, 673, 674, 675 )
UPDATE BDR.Variable SET NeedsMapping = 0 WHERE VarId IN ( 486 )
UPDATE BDR.Variable SET IsLabData = 1 WHERE VarId IN ( 527,528,529,530,531,532,533,535,536,537,538,539, 540 )
UPDATE BDR.Variable SET IsLabData = 1 WHERE VarId IN (762,764,766,768,770,518,771,750,751,752,756 )

SELECT * FROM BDR.VariablesThatNeedMapping WHERE IsLabData IS NULL ORDER BY ContextId, VarId;


ALTER VIEW BDR.VariableEnumMapping AS
SELECT er.RowId, v.VarId, mi.ItemId, v.VarName, vo.OptionValue, vo.OptionText, 
  mia.OptionText AS OptionTextDirect, mir.OptionText AS OptionTextMapped, 
  ISNULL(ISNULL(mia.OptionText, mir.OptionText),'') AS OptionTextFinal
FROM BDR.VariableOption vo 
JOIN BDR.Variable v ON v.VarId = vo.VarId AND v.IsCountry = 0
JOIN MetaItem mi ON mi.ItemId =  v.ItemId
LEFT JOIN MetaItemAnswer mia ON mia.ItemId = mi.ItemId AND mia.EnumVal = vo.OptionValue
LEFT JOIN BDR.EnumReplacement er ON er.ItemId = v.ItemId AND er.TheirEnumVal = vo.OptionValue
LEFT JOIN MetaItemAnswer mir ON mir.ItemId = v.ItemId AND mir.EnumVal = er.OurEnumVal;


