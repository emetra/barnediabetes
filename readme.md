## Barnediabetes

### Import til QA-milj�


QA-milj�ene er tilgjengelige fra ekstern-rdp i Helse Vest IKT sitt nett, men er ikke tilgjengelig i Helse Nord eller Helse S�r-�st.
Det samme gjelder filoverf�ringstjenesten (https://fot.nhn.no).  Problemet er ikke at MRS og FOT ikke er tilgjengelig i seg selv,
men at BankID og HelseID ikke er tilgjengelig (trafikk ikke tillatt).

https://mrs.qa.nhn.no/barnediabetesregister-v2/tools/formimportv2