CREATE PROCEDURE BDR.GetCaseListEyeCheck(@StudyId INT) AS
BEGIN
  SELECT v.*, CONCAT('�yelege ', ISNULL( CONVERT(VARCHAR, FORMAT(eyedate.DTVal, 'dd.MM.yyyy') ), 'Mangler') ) as InfoText 
    FROM dbo.ViewActiveCaseListStub v
  LEFT JOIN dbo.GetLastDateTable(3323, NULL) dxdate on dxdate.PersonId = v.PersonId 	AND ( dxdate.DTVal < GetDate() - 365 * 5)
  LEFT JOIN dbo.GetLastDateTable(3354, NULL) eyedate on eyedate.PersonId = v.PersonId
  WHERE StudyId = @StudyId
    AND ( DOB < GetDate() - 365 * 11 )
    AND ( (eyedate.DTVal IS NULL) OR (eyedate.DTVal < GetDate() - 365 * 2) );
END