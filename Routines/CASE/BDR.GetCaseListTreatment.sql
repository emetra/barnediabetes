CREATE PROCEDURE BDR.GetCaseListTreatment( @StudyId INT ) AS
BEGIN
  SELECT v.*, CONCAT(
    CASE
      WHEN apparat.EnumVal IS NULL THEN 'Pumpe/penn ukjent.'
      WHEN apparat.EnumVal = 1 THEN apparat.OptionText
      WHEN apparat.EnumVal = 2 AND pumpe.EnumVal IS NOT NULL THEN pumpe.OptionText + '.'
      WHEN apparat.EnumVal = 2 THEN apparat.OptionText 
      ELSE CONCAT('Ukjent alternativ: ', apparat.OptionText, '.') END, ' ',
    CASE
      WHEN eigenmåling.EnumVal IS NULL THEN 'CGM ukjent'
      WHEN eigenmåling.EnumVal = 1 AND cgm.EnumVal IS NOT NULL THEN cgm.OptionText
      WHEN eigenmåling.EnumVal = 1 THEN eigenmåling.OptionText
      ELSE 'Ikke CGM' END, '.')
    AS InfoText
  FROM dbo.ViewActiveCaseListStub v
  LEFT JOIN dbo.GetLastEnumValuesTable( 4056, NULL ) apparat ON apparat.PersonId = v.PersonId
  LEFT JOIN dbo.GetLastEnumValuesTable( 1310, NULL ) eigenmåling ON eigenmåling.PersonId = v.PersonId
  LEFT JOIN dbo.GetLastEnumValuesTable( 5162, NULL ) pumpe ON pumpe.PersonId = v.PersonId
  LEFT JOIN dbo.GetLastEnumValuesTable( 5166, NULL ) cgm ON cgm.PersonId = v.PersonId
  WHERE v.StudyId = @StudyId
  ORDER BY v.DOB, v.FullName;
END