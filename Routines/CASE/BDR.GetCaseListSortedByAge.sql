CREATE PROCEDURE BDR.GetCaseListSortedByAge( @StudyId INT ) AS
BEGIN
  SET LANGUAGE NORWEGIAN;
  SELECT v.*,
    FORMAT( DATEDIFF( DD, v.DOB, GETDATE() - 15 ) / 365.25, 'Alder ~ 0.# �r' ) AS InfoText
  FROM dbo.ViewActiveCaseListStub v
  WHERE v.StudyId = @StudyId
  ORDER BY DOB;
END