CREATE PROCEDURE BDR.GetDebutAndType AS
BEGIN
  SELECT NationalId, EventTime, NDV_TYPE, BDR_DIAGNOSE
  FROM
  (
    SELECT p.NationalId, ce.EventTime, cdp1.EnumVal AS NDV_TYPE, cdp2.DTVal AS BDR_DIAGNOSE,
      ROW_NUMBER() OVER (PARTITION BY p.NationalId ORDER BY ce.EventTime DESC ) AS ReverseOrder  
    FROM dbo.ClinEvent ce 
    JOIN dbo.Person p ON p.PersonId = ce.PersonId
    JOIN dbo.ClinDataPoint cdp1 ON cdp1.EventId = ce.EventId AND cdp1.ItemId = 3196
    JOIN dbo.ClinDataPoint cdp2 ON cdp2.EventId = ce.EventId AND cdp2.ItemId = 3323
  ) agg
  WHERE ( agg.ReverseOrder = 1 ) AND ( DATALENGTH( NationalId ) = 11 );
END


