<Query Kind="Program">
  <Connection>
    <ID>fcde096f-ba1c-446d-a381-f83de3c79f07</ID>
    <NamingServiceVersion>2</NamingServiceVersion>
    <Persist>true</Persist>
    <Server>LOCALHOST</Server>
    <AllowDateOnlyTimeOnly>true</AllowDateOnlyTimeOnly>
    <DeferDatabasePopulation>true</DeferDatabasePopulation>
    <Database>GBD</Database>
  </Connection>
  <Output>DataGrids</Output>
  <NuGetReference>Microsoft.Office.Interop.Excel</NuGetReference>
  <Namespace>Microsoft.Office.Interop.Excel</Namespace>
</Query>

void Main()
{
	string sqlScript = $" -- Output from {sheetNo}" + (char)10;
	Application xlApp = new Application();
	Workbook xlWorkbook = xlApp.Workbooks.Open(@"C:\work\Azure.FastTrakDownloads\BARNEDIABETES\Documents\BDR Kodebok November 2019.xlsx");
	try
	{
		Worksheet xlWorksheet = xlWorkbook.Sheets[sheetNo];
		Microsoft.Office.Interop.Excel.Range xlRange = xlWorksheet.UsedRange;
		string varName = "";
		string itemType = "";
		string varText = "";
		string scriptLine = "";
		
	  for ( int i = 2; i <= xlRange.Rows.Count; i++ )
	  {
		var possibleVarName = xlRange.Cells[i,varNameCol].Value2.ToString();
		if ( possibleVarName != "" ) 
		{
		  varName = possibleVarName.Trim();
		  try
		  {
		    itemType = xlRange.Cells[ i, varTypeCol ].Value2.ToString().Trim();
		  }
		  catch 
		  {
		    itemType = "Uspesifisert";
		  }
		  varText = xlRange.Cells[ i, varTextCol ].Value2.ToString().Trim();
		  scriptLine = "EXEC BDR.AddItem2019 '"+ varText  +"','" + varName + "', '" + itemType + "', " + contextNo + ", " + i + ";";
		  sqlScript = sqlScript + scriptLine + (char)10;
		  LINQPad.Extensions.Dump( scriptLine );
		}
		else if ( itemType == "Enkeltvalg" )
		{
		  var itemOption = xlRange.Cells[ i, itemOptionCol ].Value2.ToString();
		  if ( itemOption != "" ) 
		  {
		  	scriptLine = "EXEC BDR.AddItemOption '" + varName + "', " + itemOption.Replace( " = ", ", '" ) + "';";
			sqlScript = sqlScript + scriptLine + (char)10;
			LINQPad.Extensions.Dump( scriptLine );
		  }
		}
		var fileName = $"c:\\work\\Azure.FastTrakDownloads\\Barnediabetes\\BdrScript{contextNo}.sql";
		File.WriteAllText( fileName,  sqlScript );
	  }
	}
	finally
	{
		xlWorkbook.Close();
	}
}

// 1 = Famileanamnese, 2 = Pasientsvar foresatte , 3 = Pasientsvar barn , 4 = Årskontroll, 5 = Førstegangsregistrering
private readonly int sheetNo = 1; 
private readonly int varTextCol = 6;
private readonly int varNameCol = 5;
private readonly int itemOptionCol = 10;
private readonly int varTypeCol = 11;
private readonly int contextNo = 20192;