-- Dette er datasettet som skal pivoteres i kode:

SELECT ce.PersonId, cf.ClinFormId, 
  CONVERT(DATE,ce.EventTime) AS EventDate, mi.VarName, 
  cdp.EnumVal, cdp.Quantity, cdp.DTVal, cdp.TextVal
FROM dbo.ClinForm cf
JOIN dbo.ClinDataPoint cdp ON cdp.EventId = cf.EventId
JOIN dbo.ClinEvent ce ON ce.EventId = cf.EventId
JOIN dbo.MetaItem mi ON mi.ItemId = cdp.ItemId
JOIN dbo.MetaForm mf ON mf.FormId = cf.FormId
WHERE mf.FormName = 'BDR_YEAR_2021'
ORDER BY ce.PersonId, mi.VarName;

-- I tillegg skal det v�re med kobling mot hovedskjema
-- Vi m� ogs� markere registervariabler spesielt, men det kan ogs� gj�res som i NDV med en liste av id-er.