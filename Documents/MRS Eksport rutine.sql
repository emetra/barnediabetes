SELECT * FROM MetaItemType
GO

ALTER PROCEDURE BDR.ExportToMrs( @FormName VARCHAR(32) ) AS
BEGIN
  SELECT 
    p.NationalId, cf.ClinFormId, mi.ItemType, cdp.ItemId,mi.VarName, cdp.EnumVal, cdp.Quantity, cdp.DTVal, cdp.TextVal
  FROM dbo.ClinForm cf
    JOIN dbo.ClinDataPoint cdp ON cdp.EventId = cf.EventId
    JOIN dbo.ClinEvent ce on ce.EventId = cf.EventId
    JOIN dbo.MetaForm mf ON mf.FormId = cf.FormId
    JOIN dbo.MetaItem mi ON mi.ItemId = cdp.ItemId
    JOIN dbo.MetaFormItem mfi ON mfi.FormId = cf.FormId AND mfi.ItemId = cdp.ItemId
    JOIN dbo.Person p ON p.PersonId = ce.PersonId
  WHERE mf.FormName = @FormName AND DATALENGTH(p.NationalId) = 11;
END
GO
    

